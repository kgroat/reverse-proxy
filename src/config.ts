
import * as path from 'path'

export const IS_DEV = process.env.NODE_ENV !== 'production'
export const DEV_IP = process.env.DEV_IP || '127.0.0.1'

export const LISTEN_PORT = parseInt(process.env.PORT!) || 3000

export const DELUGE_PORT = parseInt(process.env.DELUGE_PORT!) || 8112
export const PLEX_PORT = parseInt(process.env.PLEX_PORT!) || 32400
export const SONARR_PORT = parseInt(process.env.SONARR_PORT!) || 8989
export const RADARR_PORT = parseInt(process.env.RADARR_PORT!) || 7878
export const PONG_PORT = parseInt(process.env.PONG_PORT!) || 80
export const PONG_PATH = process.env.PONG_PATH || '/ping'
export const PONG_MATCH = new RegExp(process.env.PONG_MATCH || '^pong$')

export const STATIC_DIR = path.join(__dirname, '../static')

export const DELUGE_APP = 'DELUGE'
export const PLEX_APP = 'PLEX'
export const SONARR_APP = 'SONARR'
export const RADARR_APP = 'RADARR'

export type AppName = typeof DELUGE_APP | typeof PLEX_APP | typeof SONARR_APP | typeof RADARR_APP

export const portMap = {
  [DELUGE_APP]: DELUGE_PORT,
  [PLEX_APP]: PLEX_PORT,
  [SONARR_APP]: SONARR_PORT,
  [RADARR_APP]: RADARR_PORT,
}
