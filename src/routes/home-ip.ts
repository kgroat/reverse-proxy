
import { Router } from 'express'
import { getClientIp } from 'request-ip'

import { getIp, setIp } from '../ipUtils'
import { WAITING_FOR_IP } from './statics'
import { handleErrors } from '../handleErrors'

export const homeIp = Router()

homeIp.get('/', handleErrors(async (_req, res) => {
  const ip = await getIp()
  if (ip === null) {
    res.setHeader('Content-Type', 'text/html')
    res.send(WAITING_FOR_IP)
    return
  }
  res.send(ip)
}))

homeIp.post('/', handleErrors(async (req, res) => {
  const clientIp = getClientIp(req)
  setIp(clientIp)
  res.send(clientIp)
}))
