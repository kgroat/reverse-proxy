
import { Router } from 'express'
import * as path from 'path'
import * as fs from 'fs'

import { STATIC_DIR } from '../config'
import { handleErrors } from '../handleErrors'

export const FAVICON = fs.readFileSync(path.join(STATIC_DIR, 'favicon.ico'))
export const WAITING_FOR_IP = fs.readFileSync(path.join(STATIC_DIR, 'waiting-for-ip.html'))
export const CHOOSE_APP = fs.readFileSync(path.join(STATIC_DIR, 'choose-app.html'))

export const statics = Router()

statics.get('/favicon.ico', handleErrors(async (_req, res) => {
  res.send(FAVICON)
}))

statics.get('/waiting-for-ip', handleErrors(async (_req, res) => {
  res.setHeader('Content-Type', 'text/html')
  res.send(WAITING_FOR_IP)
}))

