
import { Router } from 'express'

import { CHOOSE_APP } from './statics'
import { handleErrors } from '../handleErrors'

export const chooseApp = Router()

chooseApp.get('/', (_req, res) => {
  res.setHeader('Content-Type', 'text/html')
  res.send(CHOOSE_APP)
})
