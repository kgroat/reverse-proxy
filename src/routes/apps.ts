
import { Router } from 'express'
import * as httpProxy from 'http-proxy'
import { getIp } from '../ipUtils'

import { portMap, AppName, PLEX_APP, DELUGE_APP, RADARR_APP, SONARR_APP } from '../config'
import { WAITING_FOR_IP } from './statics'
import { handleErrors } from '../handleErrors'
import { ProxyError } from '../errors/ProxyError'

export const apps = Router()

interface AppHandlerOptions {
  proxyOpts?: httpProxy.ServerOptions
  pathPrefix?: string
}

const apiProxy = httpProxy.createProxyServer()
function appHandler (app: AppName, opts: AppHandlerOptions = {}) {
  const { proxyOpts, pathPrefix = '/' } = opts
  return handleErrors(async (req, res, next) => {
    console.info(`(${app}) Responding to: ${req.method} ${req.url}`)
    const ip = await getIp()
    if (ip === null) {
      console.info(`Still waiting for IP`)
      res.setHeader('Content-Type', 'text/html')
      res.send(WAITING_FOR_IP)
      return
    }
    
    const port = portMap[app]
    console.info(`Using IP and port ${ip}:${port}`)
    apiProxy.web(req, res, { ...proxyOpts, target: `http://${ip}:${port}${pathPrefix}` }, (err) => {
      if (err) {
        console.warn('something went wrong while proxying:', err)
        next(new ProxyError(req.url, err))
        return
      }
    })
  })
}

apps.use('/deluge/', appHandler(DELUGE_APP, {
  proxyOpts: {
    headers: {
      'X-Deluge-Base': '/deluge',
      'X-Frame-Options': 'SAMEORIGIN',
    },
    cookiePathRewrite: {
      '/': '/deluge/',
    },
  },
}))

apps.use('/sonarr/', appHandler(SONARR_APP, {
  pathPrefix: '/sonarr/',
}))

apps.use('/radarr/', appHandler(RADARR_APP, {
  pathPrefix: '/radarr/',
}))

apps.use('/', appHandler(PLEX_APP))
