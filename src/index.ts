
import * as express from 'express'
import * as cookieParser from 'cookie-parser'

import { chooseApp } from './routes/choose-app'
import { apps } from './routes/apps'
import { homeIp } from './routes/home-ip'
import { statics } from './routes/statics'
import { LISTEN_PORT } from './config'
import { gracefulHandler } from './handleErrors'

const app = express()

// Middleware
app.use(cookieParser())

// Routers
app.use('/', statics)
app.use('/choose-app', chooseApp)
app.use('/home-ip', homeIp)
app.use('/', apps)

// Error handlers
app.use(gracefulHandler)

app.listen(LISTEN_PORT, () => {
  console.log(`listening on port ${LISTEN_PORT}`)
})
