
import fetch from 'node-fetch'
import { IS_DEV, PONG_PORT, PONG_PATH, PONG_MATCH, DEV_IP } from './config'

let ip: string | null = IS_DEV ? DEV_IP : null

async function testIp (_newIp: string): Promise<boolean> {
  const response = await fetch(`http://${_newIp}:${PONG_PORT}${PONG_PATH}`)
  const data = await response.text()
  return response.ok && PONG_MATCH.test(data)
}

export async function setIp (newIp: string) {
  if (ip === newIp) {
    return
  }

  console.info(`Received new IP ${newIp}`)
  console.info('Testing IP for validity')
  const isValidIp = await testIp(newIp)
  if (isValidIp) {
    console.info(`New IP ${newIp} was found to be valid`)
    ip = newIp
  } else {
    console.error(`New IP ${newIp} was found to be invalid!`)
  }
}

export async function getIp () {
  return ip
}
