
export class ProxyError extends Error {
  constructor(public url: string, public cause?: any) {
    super(`Proxy to ${url} failed`)
  }
}
