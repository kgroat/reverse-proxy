
import { RequestHandler, ErrorRequestHandler } from 'express'
import { ProxyError } from './errors/ProxyError'
import { IS_DEV } from './config'

export const gracefulHandler: ErrorRequestHandler = (err, _req, res) => {
  if (err instanceof ProxyError) {
    console.warn('Unhandled proxy error:', err.cause)
    res.sendStatus(503)
    if (IS_DEV && err.cause) {
      res.send(err.cause.stack || err.cause)
    } else {
      res.send(err.message)
    }
  } else {
    console.warn(`Unhandled error:`, err && err.cause || err)
    res.sendStatus(500)
    if (IS_DEV && err && err.cause) {
      res.send(err.cause.stack || err.cause)
    } else if (IS_DEV && err) {
      res.send(err.stack || err)
    } else {
      res.send('Something went wrong')
    }
  }
}

export const handleErrors = (origFn: RequestHandler): RequestHandler => (req, res, next) => {
  const result = origFn(req, res, next)
  if (typeof result.catch === 'function') {
    result.catch(next)
  }
}
