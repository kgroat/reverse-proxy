#!/usr/bin/env node

const path = require('path')
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

require('ts-node').register({
  project: path.join(__dirname, 'tsconfig.json'),
})

require('./src/index.ts')
